from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import os

class WebDriverFactory():

    def __init__(self, browser):

        self.browser = browser

    def getDriverInstance(self):

        baseURL = "https://signup.insly.com/signup"
        if self.browser == 'Chrome':
             #set the local chrome driver path here
             dir = os.path.dirname("C:")
             chrome_driver_path = dir + "\chromedriver.exe"
             driver = webdriver.Chrome()

        elif self.browser == 'firefox':
            #set the local  geckodriver path here
             binary = FirefoxBinary('C:\\Program Files\\Mozilla Firefox\\firefox.exe')
             driver = webdriver.Firefox()

        else:
             driver = webdriver.Firefox()

        driver.implicitly_wait(10)
        driver.maximize_window()
        driver.get(baseURL)

        return driver