from base.common_methods import SeleniumDriver
import utilities.custom_logger as cl
import logging
import time

class SignUp(SeleniumDriver):

    log = cl.customLogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Locators #

    broker_company_name = "broker_name"
    broker_country_address = "broker_address_country"
    broker_company_profile = "prop_company_profile"
    broker_number_of_employee = "prop_company_no_employees"
    broker_company_description = "prop_company_person_description"
    broker_admin_email = "broker_admin_email"
    broker_admin_name = "broker_admin_name"
    suggest_broker_person_password = "suggest a secure password"
    ok_button = "//div[@class='ui-dialog-buttonset']"
    broker_admin_phone = "broker_admin_phone"
    agree_terms_and_conditions = "terms and conditions"
    i_agree_button = "//div[@class='ui-dialog-buttonset']/button[@class='primary']"
    privacy_policy_link = "privacy policy"
    scroll_down = "//div[contains(text(),'Revision: 1.20180525')]"
    close_popup = "/html/body/div[2]/div[1]/a"
    agree_privacy_policy = "//*[@id='field_terms']/td[2]/div/div/label[2]"
    agree_data_processing = "//*[@id='field_terms']/td[2]/div/div/label[3]"
    click_agree_termsandconditions = "//*[@id='field_terms']/td[2]/div/div/label[1]/span"
    signup_button = "submit_save"

    #Element Interaction #

    def enterBrokerName(self, bcn):
        self.sendKeys(bcn, locator=self.broker_company_name)
        time.sleep(1)

    def selectBrokerCountry(self):
        self.selectDropdown(option_to_select="IN", byValue=True, locator=self.broker_country_address,locatorType="id", timeToWait=1)
        time.sleep(1)

    def selectBrokerCompanyProfile(self):
        self.selectDropdown(option_to_select="IA", byValue=True, locator=self.broker_company_profile,locatorType="id", timeToWait=1)
        time.sleep(1)

    def selectNumberOfEmployee(self):
        self.selectDropdown(option_to_select="20", byValue=True, locator=self.broker_number_of_employee,locatorType="id", timeToWait=1)
        time.sleep(1)

    def selectBrokerCompanyDescription(self):
        self.selectDropdown(option_to_select="tech", byValue=True, locator=self.broker_company_description,locatorType="id", timeToWait=1)
        time.sleep(1)

    def enterBrokerAdminEmail(self, bae):
        self.sendKeys(bae, locator=self.broker_admin_email)
        time.sleep(1)

    def enterBrokerAdminName(self, ban):
        self.sendKeys(ban, locator=self.broker_admin_name)
        time.sleep(1)

    def suggestBrokerAdminPassword(self):
        self.elementClick(locator=self.suggest_broker_person_password,locatorType="link")
        time.sleep(1)

    def cliclOkButton(self):
        self.elementClick(locator=self.ok_button,locatorType="xpath")
        time.sleep(1)

    def enterBrokerAdminPhone(self, baph):
        self.sendKeys(baph, locator=self.broker_admin_phone)
        time.sleep(1)

    def selectClickTC(self):
        self.elementClick(locator=self.click_agree_termsandconditions,locatorType="xpath")
        time.sleep(1)

    def clickPrivaycPolicyLink(self):
        self.elementClick(locator=self.privacy_policy_link,locatorType="link")
        time.sleep(2)

    def scrollDown(self):
        self.elementClick(locator=self.scroll_down,locatorType="xpath")
        time.sleep(2)

    def clickClosePopUP(self):
        self.elementClick(locator=self.close_popup,locatorType="xpath")
        time.sleep(1)

    def selectPrivacyPlociy(self):
        self.elementClick(locator=self.agree_privacy_policy,locatorType="xpath")
        time.sleep(1)

    def selectAgreeDataProcessing(self):
        self.elementClick(locator=self.agree_data_processing,locatorType="xpath")
        time.sleep(1)

    def selectAgreeTermsAndConditions(self):
        self.elementClick(locator=self.agree_terms_and_conditions,locatorType="link")
        time.sleep(2)

    def clickIagreeBUtton(self):
        self.elementClick(locator=self.i_agree_button,locatorType="xpath")
        time.sleep(1)

    def clickSignUPButton(self):
        self.elementClick(locator=self.signup_button,locatorType="id")

    #Sign Up Method #

    def signUp(self, bcn="", bae="", ban="", baph=""):
        self.enterBrokerName(bcn)
        self.selectBrokerCountry()
        self.selectBrokerCompanyProfile()
        self.selectNumberOfEmployee()
        self.selectBrokerCompanyDescription()
        self.enterBrokerAdminEmail(bae)
        self.enterBrokerAdminName(ban)
        self.suggestBrokerAdminPassword()
        self.cliclOkButton()
        self.enterBrokerAdminPhone(baph)
        self.webScroll(direction="down")
        self.selectAgreeTermsAndConditions()
        self.clickIagreeBUtton()
        self.clickPrivaycPolicyLink()
        self.scrollDown()
        self.clickClosePopUP()
        self.selectPrivacyPlociy()
        self.selectAgreeDataProcessing()
        self.selectClickTC()
        self.clickSignUPButton()

    def verifyTitle(self):
        if "Insly" in self.getTitle():
         return True
        else:
         return False
