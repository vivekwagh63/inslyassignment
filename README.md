# InslyAssignment

# Required Softwares
*  Pycharm latest version
*  python 3.5 interpreter or above
*  Windows 7 or above
*  geckodriver - set environment variable path for it
*  chromedriver - set environment variable pathfor it.

# Packages in interpreter
* Appium-Python-Client	0.34	0.34
* EasyProcess	0.2.3	0.2.5
* PyVirtualDisplay	0.2.1	0.2.1
* PyYAML	3.13	4.2b4
* Pygments	2.2.0	2.3.1
* Wand	0.4.4	
* aenum	2.0.8	2.1.2
* apipkg	1.5	1.5
* appnope	0.1.0	0.1.0
* atomicwrites	1.2.1	1.2.1
* attrs	18.1.0	18.2.0
* backcall	0.1.0	0.1.0
* beautifulsoup4	4.6.3	4.7.1
* boto	2.49.0	2.49.0
* certifi	2018.10.15	2018.11.29
* chardet	3.0.4	3.0.4
* constants	0.6.0	0.6.0
* coverage	4.5.1	5.0a4
* decorator	4.2.1	4.3.0
* docker	2.7.0	3.7.0
* docker-pycreds	0.4.0	0.4.0
* dotmap	1.2.20	1.3.4
* et-xmlfile	1.0.1	
* execnet	1.5.0	1.5.0
* flake8	3.5.0	3.6.0
* gevent	1.2.2	1.4.0
* greenlet	0.4.15	0.4.15
* idna	2.6	2.8
* ipdb	0.11	0.11
* ipython	6.3.0	7.2.0
* ipython-genutils	0.2.0	
* jdcal	1.4	1.4
* jedi	0.11.1	0.13.2
* linecache2	1.0.0	1.0.0
* mccabe	0.6.1	0.6.1
* monotonic	1.5	1.5
* more-itertools	4.1.0	5.0.0
* nose	1.3.7	1.3.7
* numpy	1.15.2	1.16.0
* openpyxl	2.5.9	2.6.0b1
* pandas	0.23.4	0.24.0rc1
* parameterized	0.6.1	0.6.1
* parso	0.1.1	0.3.1
* pexpect	4.4.0	4.6.0
* pickleshare	0.7.4	0.7.5
* pip	9.0.3	18.1
* pluggy	0.8.0	0.8.1
* prompt-toolkit	1.0.15	
* ptyprocess	0.5.2	0.6.0
* py	1.5.3	1.7.0
* pycodestyle	2.3.1	2.4.0
* pyflakes	1.6.0	2.0.0
* pyotp	2.2.6	2.2.7
* pytest	3.9.1	4.1.1
* pytest-cov	2.6.0	2.6.1
* pytest-forked	0.2	1.0.1
* pytest-html	1.19.0	1.20.0
* pytest-metadata	1.7.0	1.8.0
* pytest-rerunfailures	4.2	6.0
* pytest-xdist	1.23.2	1.26.0
* python-dateutil	2.7.3	2.7.5
* pytz	2018.5	2018.9
* requests	2.18.4	2.21.0
* requests-file	1.4.3	1.4.3
* selenium	3.8.1	3.141.0
* selenium-astride	0.3.1	0.3.1
* selenium-aurelia	0.1.1	0.1.1
* selenium-base	0.1.0	0.1.0
* selenium-chrome-screenshot	0.0.1	0.0.1
* selenium-components	0.2	0.2
* selenium-configurator	0.1	0.1
* selenium-docker	0.5.0	0.5.0
* selenium-helper	0.1.2.1	0.1.2.2
* selenium-page-elements	0.1.6	0.1.6
* selenium-page-objects	0.2.1	0.2.1
* selenium-pom	0.1.4	0.1.4
* selenium-react-select	0.0.4	0.0.4
* selenium-requests	1.3	1.3
* selenium-unittest-common	1.0.3	1.0.3
* selenium-utils	0.4	0.4
* setuptools	39.0.1	40.6.3
* simplegeneric	0.8.1	0.8.1
* six	1.11.0	1.12.0
* tenacity	4.8.0	5.0.3.dev12
* tldextract	2.2.0	2.2.0
* toolz	0.9.0	0.9.0
* traceback2	1.4.0	1.4.0
* traitlets	4.3.2	4.3.2
* unittest2	1.1.0	1.1.0
* urllib3	1.22	1.24.1
* virtualenv	15.2.0	16.2.0
* wcwidth	0.1.7	0.1.7
* websocket-client	0.54.0	0.54.0

Note: Some packages are extra.

# How to run on chrome or firefox browser

1.   run below command on terminal of pycharm
  
*  py.test test_file_name --browser browser_name

# Framework structure

1.  base directory

*  base directory will have common_methods.py and webdriverfactory.py files.

*  common_methods.py: this file will have all common methods which are used in test file.

*  webdriverfactory.py: this file will have browser and baseurl configuration

2.  pages directory

*  sign_up_page.py: this file will consist of page class for sign up page.

3.  tests directory

*  sign_up_test.py: this file will consist of test class for sign up page.

*  conftest.py : this will have one time setup for running the test cases.

4.  libs directory: it will have driver required for firefox browser.

5.  utilities directory

*  checkstatus.py : this file will consist of methods which will check the status of each step of test case.

*  customlogger.py : this file will have logging infrastructure which will log every step of test case and it will store all logs
into automation.log file


     
