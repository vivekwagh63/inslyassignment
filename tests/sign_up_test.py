import unittest
from pages.sign_up_page import SignUp
import pytest
from utilities.checkstatus import CheckStatus
import time

@pytest.mark.usefixtures("oneTimeSetup","setUp")
class SignUpTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def objectSetup(self, oneTimeSetup):
        self.su = SignUp(self.driver)
        self.ts = CheckStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_signUp(self):
        time.sleep(2)
        self.su.signUp("InslyAssignment", "automation@broker.com", "broker at insly", "1122223334")
        result1 = self.su.verifyTitle()
        self.ts.mark(result1, "Title Verified")






