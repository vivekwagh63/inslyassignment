import pytest
from base.webdriverfactory import WebDriverFactory


@pytest.yield_fixture()
def setUp():
    print(" Running at method level setUp")
    yield
    print(" Running at method level tearDown")


@pytest.yield_fixture(scope="class")
def oneTimeSetup(request, browser):
    print("Running onetime setup")
    wdf = WebDriverFactory(browser)
    driver = wdf.getDriverInstance()
    if request.cls is not None:
        request.cls.driver = driver

        yield driver
        print("Running One time tear down")


def pytest_addoption(parser):
    parser.addoption("--browser")
    parser.addoption("--osType", help="Type of operating system")


@pytest.fixture(scope="session")
def browser(request):
    return request.config.getoption("--browser")


@pytest.fixture(scope="session")
def osType(request):
    return request.config.getoption("--osType")
